/**
 * CS 5133: Data Networks
 * Project 2
 *
 * Author: Morgan Robertson
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include "robertsonP2Util.h"

void sendReceive(const void *writeBuf, size_t *writeLen, void *readBuf,
                 size_t *readLen, int connSock);

int main(int argc, char *argv[]) {
    in_port_t serverPort = 0;
    in_port_t receiverPort = 0;
    int connSock = 0;
    int status = 0;
    int bytesWritten = 0;
    int bytesRead = 0;
    size_t readLen = 0;
    size_t writeLen = 0;
    char username[256] = "";
    char password[256] = "";
    char receiver[256] = "";
    char port[8] = "";
    char message[256] = "";
    char buffer[256] = "";

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <server> <port>\n", argv[0]);
        exit(1);
    }

    serverPort = atoi(argv[2]);

    // Address information for the server
    struct sockaddr_in serverAddr;
    socklen_t addrSize = sizeof(serverAddr);
    memset(&serverAddr, 0, addrSize);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(serverPort);
    inet_pton(AF_INET, argv[1], &serverAddr.sin_addr.s_addr);

    // Create a socket for connecting to server
    connSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (-1 == connSock) {
        fprintf(stderr, "Failed to create socket\n");
        close(connSock);
        exit(1);
    }

    // Connect to server
    status = connect(connSock, (struct sockaddr *)&serverAddr, addrSize);
    if (-1 == status) {
        fprintf(stderr, "Connection to server failed\n");
        close(connSock);
        exit(1);
    }

    // Get username from user
    printf("Welcome\nUsername: ");
    fgets(username, sizeof(username), stdin);

    // Send username to server and get response
    writeLen = strlen(username);
    readLen = sizeof(buffer);
    sendReceive(username, &writeLen, buffer, &readLen, connSock);

    // Get password from user
    printf("Password: ");
    fgets(password, sizeof(password), stdin);

    // Send password to server and get response
    writeLen = strlen(password);
    readLen = sizeof(buffer);
    sendReceive(password, &writeLen, buffer, &readLen, connSock);
    printf("%s\n", buffer);

    if (strcmp(buffer, "Incorrect username and/or password\n") == 0) {
        close(connSock);
        exit(1);
    }

    // Get receiver from user
    printf("Receiver name: ");
    fgets(receiver, sizeof(receiver), stdin);

    // Send receiver name to server
    writeLen = strlen(receiver);
    readLen = sizeof(buffer);
    sendReceive(receiver, &writeLen, NULL, 0, connSock);

    // Get receiver port from user
    printf("Port: ");
    fgets(port, sizeof(port), stdin);

    // Send receiver port to server
    writeLen = strlen(port);
    readLen = sizeof(buffer);
    sendReceive(port, &writeLen, buffer, &readLen, connSock);
    printf("%s\n", buffer);

    if (strcmp(buffer, "Failed to connect to receiver\n") == 0) {
        close(connSock);
        exit(1);
    }

    while (1) {
        // Get message from user
        memset(message, 0, sizeof(message));
        printf("Message: ");
        fgets(message, sizeof(message), stdin);

        // Quit if user type x or q
        if (strcmp(message, "x\n") == 0 || strcmp(message, "q\n") == 0) {
            memset(message, 0, sizeof(message));
            strcpy(message, "CLOSE\n");
            writeLen = strlen(message);
            sendReceive(message, &writeLen, NULL, 0, connSock);
            break;
        }

        // Send message to receiver
        writeLen = strlen(message);
        readLen = sizeof(buffer);
        sendReceive(message, &writeLen, buffer, &readLen, connSock);
        printf("Received message:\t%s", buffer);
    }

    close(connSock);
}

/**
 * Send a message over a connection, then wait for the response.
 *
 * writeBuf - A buffer containing data to be sent.
 * writeLen - The length of writeBuf.
 * readBuf - A buffer to hold the response.
 * readLen - The length of readBuf.
 * connSock - The socket used for communication.
 */
void sendReceive(const void *writeBuf, size_t *writeLen, void *readBuf,
                 size_t *readLen, int connSock) {
    int bytesWritten = 0;

    bytesWritten = write(connSock, writeBuf, *writeLen);
    if (bytesWritten < *writeLen) {
        fprintf(stderr, "Error writing message\n");
        close(connSock);
        exit(1);
    }
    *writeLen = bytesWritten;

    if (readBuf != NULL && *readLen > 0) {
        memset(readBuf, 0, *readLen);
        //*readLen = read(connSock, readBuf, *readLen);
        *readLen = readWithTimeout(connSock, 60, readBuf, *readLen);
        if (-1 == *readLen) {
            fprintf(stderr, "Read error\n");
            close(connSock);
            exit(1);
        }
    }
}

/**
 * CS 5133: Data Networks
 * Project 2
 *
 * Author: Morgan Robertson
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>

#include "robertsonP2Util.h"

void handleClient(int connSock, struct sockaddr_in *connAddr, socklen_t connAddrSize);
int connectToReceiver(const char *name, in_port_t port);
int verifyUser(const char *username, const char *password);
int sendMessage(const void *writeBuf, size_t writeLen, int connSock);
void lookupIP(const char *name, void *dest);

int main(int argc, char *argv[]) {
    in_port_t serverPort = 0;
    int serverSock = 0;
    int connSock = 0;
    int status = 0;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <port>\n", argv[0]);
        exit(1);
    }

    serverPort = atoi(argv[1]);

    // Address information for this server
    struct sockaddr_in serverAddr;
    socklen_t addrSize = sizeof(serverAddr);
    memset(&serverAddr, 0, addrSize);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(serverPort);
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    // Address information for a client connection
    struct sockaddr_in connAddr;
    
    // Create a socket for incoming connections
    serverSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (-1 == serverSock) {
        fprintf(stderr, "Failed to create socket\n");
        exit(1);
    }

    // Bind the server socket
    status = bind(serverSock, (struct sockaddr *)&serverAddr, addrSize);
    if (-1 == status) {
        fprintf(stderr, "Failed to bind to socket\n");
        exit(1);
    }

    // Begin listening for incoming connections
    status = listen(serverSock, 5);
    if (-1 == status) {
        fprintf(stderr, "Failed to listen on socket\n");
        exit(1);
    }

    // Accept incoming connections
    while(1) {
        socklen_t connAddrSize = sizeof(connAddr);
        memset(&connAddr, 0, connAddrSize);

        connSock = accept(serverSock, (struct sockaddr *)&connAddr, &connAddrSize);
        if (-1 == connSock) {
            fprintf(stderr, "Failed to accept connection\n");
            exit(1);
        }

        // Fork a process to handle the connection
        pid_t pid = fork();
        if (pid < 0) {
            fprintf(stderr, "Fork error\n");
            exit(1);
        }
        else if (0 == pid) {
            // Child process
            printf("Client connection on port %d\n", connAddr.sin_port);
            handleClient(connSock, &connAddr, connAddrSize);
            break;
        }
    }
}

/**
 * Process a client. The username and password are obtained from the client,
 * then messages are relayed betweent the sender and receiver.
 *
 * connSock - A file descriptor for the client socket.
 * connAddr - A pointer to the client's address information.
 * connAddrSize - The size of connAddr.
 */
void handleClient(int connSock, struct sockaddr_in *connAddr, socklen_t connAddrSize) {
    in_port_t receiverPort = 0;
    int bytesRead = 0;
    int bytesWritten = 0;
    int receiverSock = 0;
    int status = 0;
    char username[256] = "";
    char password[256] = "";
    char balance[256] = "";
    char receiver[256] = "";
    char port[8] = "";
    char buffer[256] = "";

    // Read username from client
    bytesRead = readWithTimeout(connSock, 60, username, sizeof(username));
    strip(username);
    printf("Received username from client.\n");

    // Send acknowledgement
    strcpy(buffer, "Username received\n");
    sendMessage(buffer, strlen(buffer), connSock);

    // Read password from client
    bytesRead = readWithTimeout(connSock, 60, password, sizeof(password));
    strip(password);
    printf("Received password from client\n");

    // Verify username and password
    if (0 != verifyUser(username, password)) {
        strcpy(buffer, "Incorrect username and/or password\n");
        write(connSock, buffer, strlen(buffer));
        printf("Invalid login. Closing client connection.\n");
        close(connSock);
        return;
    }

    // Send the confirmation message
    strcpy(buffer, "Username and password verified\n");
    bytesWritten = write(connSock, buffer, strlen(buffer));
    if (bytesWritten < strlen(buffer)) {
        fprintf(stderr, "Write error\n");
        close(connSock);
        return;
    }

    // Read receiver name from client
    bytesRead = readWithTimeout(connSock, 60, receiver, sizeof(receiver));
    printf("Received receiver name from client\n");

    // Read receiver port from client
    bytesRead = readWithTimeout(connSock, 60, port, sizeof(port));
    printf("Received receiver port from client\n");

    receiverPort = atoi(port);
    printf("Receiver port:\t%d\n", receiverPort);

    // Connect to the receiver
    receiverSock = connectToReceiver(receiver, receiverPort);
    if (-1 == receiverSock) {
        strcpy(buffer, "Failed to connect to receiver\n");
        write(connSock, buffer, strlen(buffer));
        close(connSock);
        return;
    }

    strcpy(buffer, "Connected to receiver\n");
    sendMessage(buffer, strlen(buffer), connSock);

    // Forward message between sender and receiver
    while (1) {
        memset(buffer, 0, sizeof(buffer));
        bytesRead = readWithTimeout(connSock, 60, buffer, sizeof(buffer));
        sendMessage(buffer, bytesRead, receiverSock);
        printf("Received from client:\t%s", buffer);

        // Check for closing connection
        if (bytesRead == 0 || strcmp(buffer, "CLOSE\n") == 0) {
            break;
        }

        memset(buffer, 0, sizeof(buffer));
        bytesRead = readWithTimeout(receiverSock, 60, buffer, sizeof(buffer));
        sendMessage(buffer, bytesRead, connSock);
        printf("Received from receiver:\t%s", buffer);
    }

    close(receiverSock);
    close(connSock);
}

/**
 * Connect to the receiver.
 *
 * name - The name of the receiver to connect to.
 * port - The port of the receiver to connect to.
 */
int connectToReceiver(const char *name, in_port_t port) {
    int connSock = 0;
    int status = 0;
    char ip[256] = "";

    // Address information for the receiver
    struct sockaddr_in receiverAddr;
    socklen_t addrSize = sizeof(receiverAddr);
    memset(&receiverAddr, 0, addrSize);
    receiverAddr.sin_family = AF_INET;
    receiverAddr.sin_port = htons(port);
    lookupIP(name, ip);
    inet_pton(AF_INET, ip, &receiverAddr.sin_addr.s_addr);

    // Create a socket for connecting to receiver
    connSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (-1 == connSock) {
        fprintf(stderr, "Failed to create socket\n");
        return -1;
    }

    // Connect to receiver
    status = connect(connSock, (struct sockaddr *)&receiverAddr, addrSize);
    if (-1 == status) {
        fprintf(stderr, "Connection to receiver failed\n");
        close(connSock);
        return -1;
    }

    return connSock;
}

/**
 * Verify a username and password. If the username and password combination is
 * invalid, -1 is returned, otherwise 0 is returned.
 *
 * username - The username to look up the balance for.
 * password - The password corresponding to username.
 */
int verifyUser(const char *username, const char *password) {
    char *line = NULL;
    size_t len = 0;
    FILE *file = fopen("userList.txt", "r");
    char fileUsername[256] = "";
    char filePassword[256] = "";

    while (getline(&line, &len, file) > 0) {
        // Read username from file and compare to passed-in username
        char *tokenPtr = strtok(line, " \t");
        //strncat(fileUsername, tokenPtr, sizeof(fileUsername) - 1);
        if (0 != strcmp(username, tokenPtr)) {
            continue;
        }

        // Read password from file and compare to passed-in password
        tokenPtr = strtok(NULL, " \t\n\r\0");
        if (0 != strcmp(password, tokenPtr)) {
            fclose(file);
            return -1;
        }

        fclose(file);
        return 0;
    }

    fclose(file);
    return -1;
}

/**
 * Look up an IP address from the list of s
 *
 * name - The name of the receiver to connect to.
 * dest - A buffer to store the IP address.
 */
void lookupIP(const char *name, void *dest) {
    char *line = NULL;
    size_t len = 0;
    FILE *file = fopen("receiverList.txt", "r");
    char server[256] = "";
    char ip[256] = "";

    while (getline(&line, &len, file) > 0) {
        // Read username from file and compare to passed-in username
        char *tokenPtr = strtok(line, " \t");
        if (0 != strcmp(server, tokenPtr)) {
            continue;
        }

        // Read IP from file
        tokenPtr = strtok(NULL, " \t\n\r\0");
        strcpy(dest, tokenPtr);
        fclose(file);
        return;
    }

    fclose(file);
    strcpy(dest, name);
}

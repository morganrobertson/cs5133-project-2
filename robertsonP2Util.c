/**
 * CS 5133: Data Networks
 * Project 2
 *
 * Author: Morgan Robertson
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "robertsonP2Util.h"

/**
 * Read from a connected socket with a timeout.
 *
 * connSock - A file descriptor for the connected socket.
 * timeout_s - The timeout in seconds.
 * buffer - A buffer to store read data.
 * bufSize - The size of buffer.
 */
int readWithTimeout(int connSock, int timeout_s, void *buffer, size_t bufSize) {
    int bytesRead = 0;
    int nReady = 0;
    fd_set read_set;
    struct timeval timeout;
    timeout.tv_sec = timeout_s;
    timeout.tv_usec = 0;

    char readChar = '\0';
    while (readChar != '\n' && bytesRead < bufSize) {
        FD_SET(connSock, &read_set);

        // Wait for message with timeout
        nReady = select(connSock + 1, &read_set, NULL, NULL, &timeout);
        if (-1 == nReady) {
            fprintf(stderr, "Read error\n");
            close(connSock);
            exit(1);
        }
        else if (0 == nReady) {
            fprintf(stderr, "Client connection timed out\n");
            close(connSock);
            exit(1);
        }

        bytesRead += read(connSock, (void *)&readChar, 1);
        if (-1 == bytesRead) {
            fprintf(stderr, "Read error\n");
            close(connSock);
            exit(1);
        }
        else if (0 == bytesRead) {
            break;
        }

        //printf("Read char:\t%c\t(%X)\n", readChar, readChar);
        memcpy(buffer + bytesRead - 1, &readChar, 1);
    }

    return bytesRead;
}

/**
* Send a message.
*
* writeBuf - The message to be sent.
* writeLen - The length of writeBuf.
* connSock - The socket to use for communication.
*/
int sendMessage(const void *writeBuf, size_t writeLen, int connSock) {
    int bytesWritten = 0;

    bytesWritten = write(connSock, writeBuf, writeLen);
    if (bytesWritten < writeLen) {
        fprintf(stderr, "Error writing message\n");
        close(connSock);
        exit(1);
    }
    return bytesWritten;
}

/**
 * Strip trailing whitespace from a string.
 *
 * s - A string to strip.
 */
void strip(char *s) {
    size_t i;
    for (i = strlen(s) - 1; i > 0; i--) {
        if (s[i] == ' ' || s[i] == '\t' || s[i] == '\n' || s[i] == '\r') {
            s[i] = '\0';
        }
        else {
            break;
        }
    }
}

/**
 * CS 5133: Data Networks
 * Project 2
 *
 * Author: Morgan Robertson
 */

int readWithTimeout(int connSock, int timeout_s, void *buffer, size_t bufSize);
int sendMessage(const void *writeBuf, size_t writeLen, int connSock);
void strip(char *s);

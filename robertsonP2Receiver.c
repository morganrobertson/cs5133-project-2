/**
 * CS 5133: Data Networks
 * Project 2
 *
 * Author: Morgan Robertson
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include "robertsonP2Util.h"

void handleClient(int connSock, struct sockaddr_in *connAddr, socklen_t connAddrSize);
int longestSubstr(const char *s1, const char *s2, char *buffer, size_t bufLen);

int main(int argc, char *argv[]) {
    in_port_t serverPort = 0;
    int serverSock = 0;
    int connSock = 0;
    int status = 0;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <port>\n", argv[0]);
        exit(1);
    }

    serverPort = atoi(argv[1]);

    // Address information for this server
    struct sockaddr_in serverAddr;
    socklen_t addrSize = sizeof(serverAddr);
    memset(&serverAddr, 0, addrSize);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(serverPort);
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    // Address information for a client connection
    struct sockaddr_in connAddr;
    
    // Create a socket for incoming connections
    serverSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (-1 == serverSock) {
        fprintf(stderr, "Failed to create socket\n");
        exit(1);
    }

    // Bind the server socket
    status = bind(serverSock, (struct sockaddr *)&serverAddr, addrSize);
    if (-1 == status) {
        fprintf(stderr, "Failed to bind to socket\n");
        exit(1);
    }

    // Begin listening for incoming connections
    status = listen(serverSock, 5);
    if (-1 == status) {
        fprintf(stderr, "Failed to listen on socket\n");
        exit(1);
    }

    // Accept incoming connections
    while(1) {
        socklen_t connAddrSize = sizeof(connAddr);
        memset(&connAddr, 0, connAddrSize);

        connSock = accept(serverSock, (struct sockaddr *)&connAddr, &connAddrSize);
        if (-1 == connSock) {
            fprintf(stderr, "Failed to accept connection\n");
            exit(1);
        }

        // Fork a process to handle the connection
        pid_t pid = fork();
        if (pid < 0) {
            fprintf(stderr, "Fork error\n");
            exit(1);
        }
        else if (0 == pid) {
            // Child process
            printf("Client connection on port %d\n", connAddr.sin_port);
            handleClient(connSock, &connAddr, connAddrSize);
            break;
        }
    }
}

/**
 * Process a client.
 *
 * connSock - A file descriptor for the client socket.
 * connAddr - A pointer to the client's address information.
 * connAddrSize - The size of connAddr.
 */
void handleClient(int connSock, struct sockaddr_in *connAddr, socklen_t connAddrSize) {
    const int MAX_MESSAGES = 32;
    int msgIdx = 0;
    int bytesRead = 0;
    int nLongest = 0;
    char readBuf[256] = "";
    char writeBuf[256] = "";
    char longest[256] = "";
    char buffer[256] = "";
    char **messages;

    // Allocate memory
    messages = (char**)malloc(MAX_MESSAGES * sizeof(char*));

    while (1) {
        memset(readBuf, 0, sizeof(readBuf));
        bytesRead = readWithTimeout(connSock, 60, readBuf, sizeof(readBuf));
        printf("Received message:\t%s\n", readBuf);

        if (strcmp(readBuf, "CLOSE\n") == 0) {
            printf("Closing client connection\n");
            close(connSock);
            break;
        }

        if (msgIdx < MAX_MESSAGES - 1) {
            strip(readBuf);
            messages[msgIdx] = (char*)malloc((strlen(readBuf)+1) * sizeof(char));
            strcpy(messages[msgIdx], readBuf);
            msgIdx++;
        }

        for (int i = 0; i < msgIdx; i++) {
            for (int j = 0; j < msgIdx; j++) {
                if (i != j){
                    int len = longestSubstr(messages[i], messages[j], buffer, sizeof(buffer));
                    if (len > nLongest) {
                        nLongest = len;
                        memset(longest, 0, sizeof(longest));
                        strcpy(longest, buffer);
                    }
                }
            }
        }

        memset(writeBuf, 0, sizeof(writeBuf));
        sprintf(writeBuf, "%s\t(length=%d)\n", longest, nLongest);
        printf("Longest common substring:\t%s\n", writeBuf);
        sendMessage(writeBuf, strlen(writeBuf), connSock);
    }

    // Free memory
    for (int i = 0; i < 64; i++) {
        free(messages[i]);
    }
    free(messages);
}

/**
 * Get the longest common substring of two strings.
 * Adapted from http://en.wikipedia.org/wiki/Longest_common_substring_problem.
 *
 * s1 - The first string to be compared.
 * s2 - The second string to be compared.
 * buffer - A buffer to store the substring.
 * bufLen - The size of buffer.
 */
int longestSubstr(const char *s1, const char *s2, char *buffer, size_t bufLen) {
    int n1 = strlen(s1);
    int n2 = strlen(s2);
    int longest = 0;
    int **count;

    // Allocate memory
    count = (int**)malloc(n1 * sizeof(int*));
    for (int i = 0; i < n1; i++) {
        count[i] = (int*)malloc(n2 * sizeof(int));
    }

    for (int i = 0; i < n1; i++) {
        for (int j = 0; j < n2; j++) {
            count[i][j] = 0;
        }
    }

    // Find the longest common substring
    for (int i = 0; i < n1; i++) {
        for (int j = 0; j < n2; j++) {
            if (s1[i] == s2[j]) {
                if (i == 0 || j == 0) {
                    count[i][j] = 1;
                }
                else {
                    count[i][j] = count[i-1][j-1] + 1;
                }

                if (count[i][j] > longest) {
                    longest = count[i][j];
                    memset(buffer, 0, bufLen);
                    strncpy(buffer, s1+i-longest+1, longest);
                }
            }
            else {
                count[i][j] = 0;
            }
        }
    }

    // Free memory
    for (int i = 0; i < n1; i++) {
        free(count[i]);
    }
    free(count);

    return longest;
}
